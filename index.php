<?php
include_once 'config/init.php';

use App\User\User;
use App\Home\Home;
use App\User\Auth;

$user = new User();
$home = new Home();
$auth = new Auth();

$resourcePath = '../resources/login-signup-modal-window/';

$status = $auth->prepare($_POST)->isLoggedIn();

$allHomes = $home->allHomes();

//var_dump('<pre>', $allHomes, '</pre>');


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?= $resourcePath ?>css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= $resourcePath ?>css/style.css"> <!-- Gem style -->
	<script src="<?= $resourcePath ?>js/modernizr.js"></script> <!-- Modernizr -->
  	
	<title>Homes</title>
	<style>
		.homelist {
			margin-top: 20px;
		}
	</style>
</head>
<body>
	<header role="banner">
		<div id="cd-logo"><a href="#0"><img src="<?= $resourcePath ?>img/cd-logo.svg" alt="Logo"></a></div>

		<nav class="main-nav">
			<ul>
				<!-- inser more links here -->
                <?php if(!$status) : ?>
				<li><a class="cd-signin" href="#0">Sign in</a></li>
				<li><a class="cd-signup" href="#0">Sign up</a></li>
                <?php else: ?>
                    <li><a href="dashboard.php">Dashboard</a></li>
                    <li><a href="auth/logout.php">Sign Out</a></li>
                <?php endif; ?>

			</ul>
		</nav>
	</header>

	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->

			<div id="cd-login"> <!-- log in form -->
				<form action="auth/login.php" method="post" class="cd-form">
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input type="email" name="email" class="full-width has-padding has-border" id="signin-email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input type="password" name="password" class="full-width has-padding has-border" id="signin-password"  placeholder="Password">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width" type="submit" value="Login">
					</p>
				</form>-->
			</div> <!-- cd-login -->

			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form" action="auth/register.php" method="post">
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-username">Username</label>
						<input type="text" name="first_name" class="full-width has-padding has-border" id="signup-username" placeholder="First Name">
						<span class="cd-error-message">Error message here!</span>
					</p>
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-username">Username</label>
						<input type="text" name="last_name" class="full-width has-padding has-border" id="signup-last"  placeholder="Last Name">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-email" for="signup-email">E-mail</label>
						<input type="email" name="email" class="full-width has-padding has-border" id="signup-email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Password</label>
						<input type="password" name="password" class="full-width has-padding has-border" id="signup-password"   placeholder="Password">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Sign Up">
					</p>
				</form>

				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-signup -->

			<a href="#0" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->

	<div class="container homelist">
		<div class="row">
			<?php foreach($allHomes as $sHome): ?>
			<div class="col-sm-6 col-md-4">
				<div class="thumbnail">
					<div class="caption">
						<h3><?= $sHome['title'] ?></h3>
						<p>Location: <?= $sHome['location'] ?></p>
						<p>Price: <?= $sHome['price'] ?> $</p>
						<p><a href="homes/single-home.php?id=<?= $sHome['id'] ?>" class="btn btn-primary" role="button">View</a></p>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>

<script src="../resources/jquery/jquery.min.js"></script>
<script src="<?= $resourcePath ?>js/main.js"></script> <!-- Gem jQuery -->
</body>
</html>