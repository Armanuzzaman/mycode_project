<?php

include_once '../config/init.php';;

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isExist();

if($status) {
    Message::message('User already exists using this email', 'danger');
    header('Location: ../index.php');
}else {
    $user->prepare($_POST)->store();
}

