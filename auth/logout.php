<?php

include_once '../config/init.php';;

use App\User\Auth;
use App\Message\Message;

$auth = new Auth();

$status = $auth->logout();
Message::message('You logged out successfully!!', 'success');
header('Location: ../index.php');