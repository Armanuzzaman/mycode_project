<?php
include_once '../config/init.php';

use App\Home\Home;
use App\User\Auth;
use App\Message\Message;

$home = new Home();
$auth = new Auth();

//var_dump($_POST);


if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){
    $image_name= time().$_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];
    move_uploaded_file( $temporary_location,'../../resources/RoomImages/'.$image_name);
    $_POST['image']=$image_name;

}
var_dump($_POST);
//die();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: ../index.php');
}

$home->prepare($_POST)->store();
